// Function
const sum1 = function (a, b) {
	return a + b
} 

// Arrow function
const sum2 = (a, b) => { return a + b }

// Implicit return
const sum3 = (a, b) => a + b

console.log(sum1(2,3))
console.log(sum2(2,3))
console.log(sum3(2,3))
