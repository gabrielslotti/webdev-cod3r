console.log('For with var...')
for (var i = 0; i < 10; i++) {
    console.log(i)
}
// It will print numbers 0 - 9
// And i after loop execution is 10

console.log('For with let...')
for (let i = 0; i < 10; i++) {
    console.log(i)
}
// Same as the first one it will print 0 - 9
// But i is undefine, cause let just look inside the execution block

// Conclusion:
// var is global and let is used just in the execution block